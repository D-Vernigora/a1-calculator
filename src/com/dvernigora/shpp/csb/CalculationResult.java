package com.dvernigora.shpp.csb;

public class CalculationResult {
    public Exception error;
    public double value;
    public CalculationResult(Object object){
        try {
            value = (double) object;
        }catch (Exception e){
            error = (Exception)object;
        }
    }
}