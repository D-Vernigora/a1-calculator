package com.dvernigora.shpp.csb;
/*
 * This program is a arithmetic calculator.*/

public class MyCalculator {
    private VDYHashMap<String, Double> variables;

    /** Method sets variables of a mathematical expression.
     * @param vars HashMap which holds variables entered by the user.
     * @return Returns true in case of successful initialization the hash map.
     */
    public boolean setVars(VDYHashMap<String, Double> vars){
        variables = vars;
        return true;
    }

    /**
     * @param args An array of strings entered by the user.
     * @return Returns HashMap which holds variables of a mathematical expression entered by the user.
     */
    public static VDYHashMap<String, Double> getVars(String[] args){
        VDYHashMap<String, Double> vars = new VDYHashMap<>();
        for (int i = 0; i < args.length; i++) {
            if (args[i].contains("=")){
                String var = args[i].substring(0, args[0].indexOf("="));
                double value = Double.parseDouble(args[i].substring(args[0].indexOf("=")+1));
                vars.put(var, value);
            }
        }
        return vars;
    }

    /**
     * @param args An array of strings entered by the user.
     * @return Returns a string stored in mathematical expression entered by the user.
     */
    public static String getMathExpression(String[] args){
        String formula = "";
        for (int i = 0; i < args.length; i++) {
            if (!args[i].contains("=")){
                formula = args[i];
            }
        }
        return formula;
    }

    /** Method responsible for calculating mathematical expression.
     * @param formula a string stored in mathematical expression entered by the user.
     * @return Returns the result of mathematical calculations.
     */
    public Object calculate(String formula){
        try {
            String mathExpression = doChangeOfVariablesToValues(variables, formula);

            while (mathExpression.contains("(")) {
                if (isFunction(mathExpression)) {
                    mathExpression = getValueOfTheFunction(mathExpression);
                } else {
                    mathExpression = removeBrackets(mathExpression);
                }
            }
            VDYArrayList<String> values = parseValues(mathExpression);
            String resOfCalculation = getResOfCalculation(values);

            return getFractionalNum(resOfCalculation);
        }catch (Exception ex){
            return ex;
        }
    }

    /**
     * @param vars HashMap which holds variables entered by the user.
     * @param formula a string stored in mathematical expression entered by the user.
     * @return Returns the mathematical expression where variables were replaced with their corresponding values.
     */
    private String doChangeOfVariablesToValues(VDYHashMap<String, Double> vars, String formula){
        String formulaTmp = formula;
        if (vars != null) {
            for (int i = 0; i < formulaTmp.length(); i++) {
                String var = formulaTmp.substring(0, i) + vars.get(formulaTmp.charAt(i) + "") + formulaTmp.substring(i + 1);
                if (i > 0 && i < formulaTmp.length() - 1) {
                    char prevCh = formulaTmp.charAt(i - 1);
                    char nextCh = formulaTmp.charAt(i + 1);
                    if (!isLetter(prevCh) && !isLetter(nextCh) && vars.containsKey(formulaTmp.charAt(i) + "")) {
                        formulaTmp = var;
                    }
                } else if (i == 0) {
                    char nextCh = formulaTmp.charAt(i + 1);
                    if (!isLetter(nextCh) && vars.containsKey(formulaTmp.charAt(i) + "")) {
                        formulaTmp = var;
                    }
                } else if (i == formulaTmp.length() - 1) {
                    char prevCh = formulaTmp.charAt(i - 1);
                    if (!isLetter(prevCh) && vars.containsKey(formulaTmp.charAt(i) + "")) {
                        formulaTmp = var;
                    }
                }
            }
        }
        return formulaTmp;
    }

    /**
     * @param formula a string stored in mathematical expression entered by the user.
     * @return Returns the mathematical expression purified from the brackets.
     */
    private String removeBrackets(String formula){
        String value = formula;
        if (value.contains("(")){
            int indOpeningBracket = value.lastIndexOf("(");
            value = value.substring(value.lastIndexOf("(")+1);
            int indClosingBracket = value.indexOf(")");
            value = value.substring(0, value.indexOf(")"));
            formula = formula.substring(0, indOpeningBracket) + getResOfCalculation(parseValues(value))
                    + formula.substring((indClosingBracket + indOpeningBracket)+2);
        }
        return formula;
    }

    /** A method that calculates the result of mathematical function and inserts it into a mathematical expression.
     * @param formula a string stored in mathematical expression entered by the user.
     * @return Returns the mathematical expression with the result of the function.
     */
    private String getValueOfTheFunction(String formula){
        if (formula.contains("(")) {
            String formulaTmp = formula;
            String function = "";
            String value = "";
            boolean isFoundVal = false;
            int indBeginVal;
            int indBeginFunc = 0;
            int indEndVal = 0;
            for (int i = formulaTmp.length() - 1; i >= 0; i--) {
                if (isLetter(formulaTmp.charAt(i)) && !isFoundVal) {
                    indBeginVal = i;
                    indBeginFunc = indBeginVal;
                    indEndVal = formulaTmp.indexOf(")") + 1;
                    formulaTmp = formulaTmp.substring(indBeginVal + 1);
                    value = formulaTmp.substring(0, formulaTmp.indexOf(")") + 1);
                    isFoundVal = true;
                }
                if (isLetter(formula.charAt(i)) && isFoundVal) {
                    while (indBeginFunc >= 0 && isLetter(formula.charAt(indBeginFunc))) {
                        function = formula.charAt(indBeginFunc) + function;
                        indBeginFunc--;
                    }
                    indBeginFunc++;
                    break;
                }
            }
            formula = formula.substring(0, indBeginFunc) + calculateValueOfTheFunction(function, removeBrackets(value)) + formula.substring(indEndVal);
        }
        return formula;
    }

    /**
     * @param function a string stored in function name.
     * @param value a string stored in the value of the function.
     * @return Returns the result of mathematical function.
     */
    private String calculateValueOfTheFunction(String function, String value){
        double res;
        double num = getFractionalNum(value);

        switch (function) {
            case "sin":
                res = Math.sin(num);
                break;
            case "abs":
                res = Math.abs(num);
                break;
            case "acos":
                res = Math.acos(num);
                break;
            case "asin":
                res = Math.asin(num);
                break;
            case "atan":
                res = Math.atan(num);
                break;
            case "cbrt":
                res = Math.cbrt(num);
                break;
            case "ceil":
                res = Math.ceil(num);
                break;
            case "cos":
                res = Math.cos(num);
                break;
            case "exp":
                res = Math.exp(num);
                break;
            case "log":
                res = Math.log(num);
                break;
            case "floor":
                res = Math.floor(num);
                break;
            case "rint":
                res = Math.rint(num);
                break;
            case "signum":
                res = Math.signum(num);
                break;
            case "sinh":
                res = Math.sinh(num);
                break;
            case "sqrt":
                res = Math.sqrt(num);
                break;
            case "tan":
                res = Math.tan(num);
                break;
            case "tanh":
                res = Math.tanh(num);
                break;
            default:
                return "";
        }
            return String.valueOf(res);
    }

    /**
     * @param formula a string stored in mathematical expression entered by the user.
     * @return Returns true if mathematical expression contains a function.
     */
    private boolean isFunction(String formula){
        if (formula.contains("(")){
            if (formula.lastIndexOf("(") != 0){
                if (isLetter(formula.charAt(formula.lastIndexOf("(")-1))){
                    return true;
                }
            }
        }
        return false;
    }

    /**The method separates the mathematical values, and mathematical operators
     * from each other and adds them to the ArrayList.
     * @param formula a string stored in mathematical expression entered by the user.
     * @return Returns ArrayList which stores mathematical values and mathematical operators.
     */
    private VDYArrayList<String> parseValues(String formula){
        VDYArrayList<String> values = new VDYArrayList<>();
        String formulaTmp = formula;

        while (isContainsOperator(formulaTmp)){
            char ch = formulaTmp.charAt(0);
            StringBuilder  value = new StringBuilder ();
            value.append(ch);
            formulaTmp = formulaTmp.substring(1);
            while (!isOperator(formulaTmp.charAt(0))){
                value.append(formulaTmp.charAt(0));
                formulaTmp = formulaTmp.substring(1);
            }
            values.add(value.toString());
            values.add(formulaTmp.charAt(0)+"");
            formulaTmp = formulaTmp.substring(1);
        }
        values.add(formulaTmp);
        return values;
    }

    /**@param values ArrayList to store the intermediate data.
     * @return Returns the result of the calculation.
     */
    private String getResOfCalculation(VDYArrayList<String> values){
        doMathAction(values, "*", "/");
        doMathAction(values, "+", "-");
        return values.get(0);
    }

    /** The method makes the necessary calculations in accordance with the rules of mathematics.
     * @param tmp Variable to store the intermediate data.
     * @param fOperator Variable to store the operator.
     * @param sOperator Variable to store the operator.
     */
    private void doMathAction(VDYArrayList<String> tmp, String fOperator, String sOperator) {
        for (int i = 0; i < tmp.size(); i++) {
            if (tmp.get(i).equals(fOperator) || tmp.get(i).equals(sOperator)){
                tmp.set(i-1, String.valueOf(getResOfMathAction(tmp.get(i-1),tmp.get(i),tmp.get(i+1))));
                tmp.remove(i);
                tmp.remove(i);
                i = 0;
            }
        }
    }

    /** Method is responsible for performing mathematical operations.
     * @param firstDigit The intermediate data.
     * @param operator The variable responsible for mathematical actions.
     * @param secondDigit The intermediate data.
     * @return Returns the result of a mathematical operation.
     */
    private double getResOfMathAction(String firstDigit, String operator, String secondDigit){
        double digitA = getFractionalNum(firstDigit);
        double digitB = getFractionalNum(secondDigit);
        double res = 0;

        if (operator.equals("*")){
            res = digitA * digitB;
        }
        if (operator.equals("/")){
            res = digitA / digitB;
        }
        if (operator.equals("+")){
            res = digitA + digitB;
        }
        if (operator.equals("-")){
            res = digitA - digitB;
        }
        return res;
    }

    /** A method that returns a match with a char equal to a mathematical operator.
     * @param ch Char from the string.
     * @return Returns true in case of a match with a mathematical operator.
     */
    private boolean isOperator(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }

    /**
     * @param formula a string stored in mathematical expression entered by the user.
     * @return Returns true if the expression contains a mathematical formula
     */
    private boolean isContainsOperator(String formula){
        return formula.contains("*") || formula.contains("/") || formula.contains("+") || formula.contains("-") &&
                formula.indexOf("-") != 0 && !isOperator(formula.charAt(formula.indexOf("-")-1));
    }

    /** The method responsible for converting the string to a number.
     * @param num The incoming data is in string format.
     * @return Returns the number format double.
     */
    private double getFractionalNum(String num) {
        return Double.parseDouble(num);
    }

    /**
     * @param data The incoming data is in char format.
     * @return Returns true, if incoming data is letter.
     */
    private boolean isLetter(char data){ return data >= 'a' && data <= 'z';}
}