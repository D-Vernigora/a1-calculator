package com.dvernigora.shpp.csb;

public class Main {
    public static void main(String[] args) {
        MyCalculator calculator = new MyCalculator();
        VDYHashMap<String, Double> vars;
        String formula;

        if (args.length > 0){
            vars = MyCalculator.getVars(args);
            formula =  MyCalculator.getMathExpression(args);
        } else {
            vars = new VDYHashMap<>();
            vars.put("a", 2.0);
            vars.put("b", 3.0);
            vars.put("c", 5.0);
            vars.put("d", 11.0);
            formula = "b*c+(abs(cos(sin(5+6)*5)*-a)*(b+c))+(d/a)";
        }

        calculator.setVars(vars);
        CalculationResult result = new CalculationResult(calculator.calculate(formula));

        if (result.error != null){
            System.out.println("We got some problems... Try to double-check your entry.");
        } else {
            System.out.println(result.value);
        }
    }
}