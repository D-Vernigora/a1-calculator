package com.dvernigora.shpp.csb;
/*
 * This program is a arithmetic calculator.*/

public class Calculator {
    public double getResultOfExpression(String[] args){
        /* Initialize arrays for intermediate results and the final */
        VDYArrayList<String> arrValues = new VDYArrayList<>();
        VDYArrayList<String> arrOperators = new VDYArrayList<>();
        VDYArrayList<String> arrOperands = new VDYArrayList<>();

        try {
            //Variable which holds the index of the array which contains the mathematical expression.
            int indMathExpression = assignValueToVariableInMatExpression(args);

            //Assign a variable the result of basic mathematical operations.
            String partOfExpression = getBasisOfExpression(args, indMathExpression, arrOperators, arrOperands, arrValues);

            //The method responsible for assigning the incoming data.
            distributeValuesAndOperators(partOfExpression, arrOperators, arrOperands, arrValues);

            //The method responsible for function evaluation from the data entered by the user.
            calculateValueOfTheFunction(arrValues);

            return getResult(arrValues);
        } catch (Exception e){
            System.out.println(e.getMessage());
            return 0;
        }
    }


    /** Method is responsible for the product of basic mathematical operations.
     * @param args An array of incoming data.
     * @param indExpression The variable responsible for the array which contains the mathematical expression.
     * @param arrOperators An array of mathematical operators.
     * @param arrTmp An array of intermediate data.
     * @param arrValues An array of results.
     */
    private String getBasisOfExpression(String[] args, int indExpression, VDYArrayList<String> arrOperators, VDYArrayList<String> arrTmp, VDYArrayList<String> arrValues) {
        // Produced by counting the number of brackets in the incoming data.
        int countParentheses = 0;
        String tmp;
        for (int i = 0; i < args[indExpression].length(); i++) {
            if (args[indExpression].charAt(i) == '(') {
                countParentheses++;
            }
        }

        // The code continues until the data is present in the incoming brace.
        if (args[indExpression].contains("(")) {
            for (int i = 0; i < countParentheses; i++) {
                tmp = args[indExpression];
                int indOpeningBracket = 0;
                int indClosingBracket;
                indClosingBracket = tmp.indexOf(')') + 1;

                // Remove the brackets until we find the expression you want to calculate first.
                while (tmp.contains(")")) {
                    indOpeningBracket += tmp.indexOf('(');
                    tmp = tmp.substring(tmp.indexOf('(') + 1, tmp.indexOf(')'));
                }
                while (tmp.contains("(")) {
                    indOpeningBracket += tmp.indexOf('(') + 1;
                    tmp = tmp.substring(tmp.indexOf('(') + 1, tmp.length());
                }

                //The method responsible for assigning the incoming data.
                distributeValuesAndOperators(tmp, arrOperators, arrTmp, arrValues);

                /* If the result of the previous actions have data with which to perform mathematical calculations,
                 * produce them.*/
                if (arrValues.size() > 1) {
                    calculateValueOfTheFunction(arrValues);
                }

                // The result of the previous action recorded in the dataset entered by the user for subsequent calculations.
                args[indExpression] = args[indExpression].substring(0, indOpeningBracket) + calculateValues(arrValues)
                        + args[indExpression].substring(indClosingBracket, args[indExpression].length());
                // Clear the arrays.
                arrTmp.clear();
                arrOperators.clear();
                arrValues.clear();
            }
        }

        // Variable in data calculations without parentheses.
        tmp = args[indExpression];

        for (int i = 0; i < tmp.length(); i++) {
            /* If in the data there are mathematical formulas,
             * that are placed in the arrays of the names of the formulas and their values.*/
            if (tmp.charAt(i) >= 'a' && tmp.charAt(i) <= 'z') {
                int funcBegin = 0;
                for (int j = 0; tmp.charAt(j) < 'a'; j++) {
                    funcBegin++;
                    if (j == tmp.length() - 1) {
                        break;
                    }
                }
                int funcEnd = funcBegin;
                for (int j = funcBegin; !isOperator(tmp.charAt(j)); j++) {
                    funcEnd++;
                    if (j != 0) {
                        if (tmp.charAt(j - 1) >= 'a' && tmp.charAt(j - 1) <= 'z') {
                            funcEnd += 1;
                            j += 1;
                        }
                    }
                    if (j > tmp.length() - 2) {
                        funcEnd = tmp.length();
                        break;
                    }
                }

                // The result of the previous action recorded in the dataset entered by the user for subsequent calculations.
                arrValues.add(tmp.substring(funcBegin, funcEnd));
                calculateValueOfTheFunction(arrValues);

                tmp = tmp.substring(0, funcBegin) + calculateValues(arrValues) + tmp.substring(funcEnd, tmp.length());
                arrValues.clear();
            }
        }
        return tmp;
    }

    /**
     * Method is responsible for turning the values into a mathematical equation instead of variables.
     * @param args An array of incoming data.
     * @return The index of the array which contains the mathematical expression.
     */
    private int assignValueToVariableInMatExpression(String[] args) {
        //Initialize the array variables and put them in it along with their meanings.
        VDYArrayList<String> variables = new VDYArrayList<>();
        int indMathExpression = 0;
        for (int i = 0; i < args.length; i++) {
            args[i] = args[i].toLowerCase();
            if (!args[i].contains("=")) {
                indMathExpression = i;
            } else {
                for (int j = 0; j < args[i].length(); j++) {
                    if (args[i].charAt(j) == '=') {
                        variables.add(args[i].substring(0, j));
                        variables.add(args[i].substring(j + 1, args[i].length()));
                    }
                }
            }
        }

        // The computed location of the variable.
        for (int i = 0; i < variables.size(); i++) {
            char data = variables.get(i).charAt(0);
            /* If array variable is found, we calculate its location in the incoming data and substitute instead,
             * the value of which it keeps.*/
            if (isLetter(data)) {
                for (int k = 0; k < args[indMathExpression].length(); k++) {
                    if (args[indMathExpression].contains(variables.get(i))) {
                        char variable = variables.get(i).charAt(0);
                        String valOfVar = variables.get(i + 1);
                        /* If the variable is not in the beginning or at the end of the row,
                         * check the presence of letters left and right from her. If not, substitute its value.*/
                        if (k != 0 && k != args[indMathExpression].length() - 1 && args[indMathExpression].charAt(k) == variable) {
                            if (!isVariable(args[indMathExpression], k-1) && !isVariable(args[indMathExpression], k+1)) {
                                args[indMathExpression] = assignValue(args[indMathExpression], valOfVar, k);
                            }
                        }
                        /* If the variable is in the beginning of the row,
                         * check the presence of letters right from her. If not, substitute its value.*/
                        if (k == 0 && args[indMathExpression].charAt(k) == variable) {
                            if (!isVariable(args[indMathExpression], k+1)) {
                                args[indMathExpression] = assignValue(args[indMathExpression], valOfVar, k);
                            }
                        }
                        /* If the variable is not in the end of the row,
                         * check the presence of letters left from her. If not, substitute its value.*/
                        if (k == args[indMathExpression].length() - 1 && args[indMathExpression].charAt(k) == variable) {
                            if (!isVariable(args[indMathExpression], k-1)) {
                                args[indMathExpression] = assignValue(args[indMathExpression], valOfVar, k);
                            }
                        }
                    }
                }
            }
        }
        return indMathExpression;
    }

    /** @return true, if incoming data is letter.
     */
    private boolean isLetter(char data){
        return data >= 'a' && data <= 'z';
    }

    /** @return true, if no presence of letters left and right from input argument.
     */
    private boolean isVariable(String arg, int ind) {
        return !(isOperator(arg.charAt(ind)) || arg.charAt(ind) == '(' || arg.charAt(ind) == ')' || arg.charAt(ind) == ',');
    }

    /** @return a string in which was replaced the variable on its value.
     */
    private String assignValue(String arg, String valOfVar, int k) {
        return arg.substring(0, k) + valOfVar + arg.substring(k + 1, arg.length());
    }

    /** @return the result of program execution.
     */
    private double getResult(VDYArrayList<String> arrValues){
        return getFractionalNum(calculateValues(arrValues));
    }


    /** If the incoming data are mathematical functions, put them into an array for further calculations.
     * @param arrValues An array of calculation results.
     */
    private void calculateValueOfTheFunction(VDYArrayList<String> arrValues) {
        VDYArrayList<String> arrFunc = new VDYArrayList<>();
        for (int j = 0; j < arrValues.size(); j++) {
            String function = "";
            String numInFunction = "";
            String trim = arrValues.get(j);
            boolean switchOf = false;
            for (int k = 0; k < trim.length(); k++) {
                if (trim.charAt(0) == '[' || trim.charAt(0) == ']' || trim.charAt(0) == ',' || trim.charAt(0) == ' '){
                    trim = trim.substring(1, trim.length());
                }
                if (trim.charAt(k) >= 'a' && trim.charAt(k) <= 'z'){
                    function += trim.charAt(k);
                    switchOf = true;
                }
                if (trim.charAt(k) >= '0' && trim.charAt(k) <= '9' && switchOf || trim.charAt(k) == '.' && switchOf
                        || trim.charAt(k) == ',' && switchOf || isOperator(trim.charAt(k))){
                    numInFunction += trim.charAt(k);
                }
            }
            if (switchOf) {
                arrFunc.add(function);
                arrFunc.add(numInFunction);
            }
            arrValues.set(j, trim);
        }

        // When a match occurs for the name of the array of functions that perform the necessary calculations.
        for (int k = 0; k < arrFunc.size(); k++) {
            boolean isFunc = true;
            double res = 0;
            double num = 0;
            double firstVal = 0;
            double secondVal = 0;

            if (arrFunc.get(k+1).contains(",")) {
                firstVal = getFractionalNum(arrFunc.get(k + 1).substring(0, arrFunc.get(k + 1).indexOf(",")));
                secondVal = getFractionalNum(arrFunc.get(k + 1).substring(arrFunc.get(k + 1).indexOf(",") + 1, arrFunc.get(k + 1).length()));
            } else {
                num = getFractionalNum(arrFunc.get(k+1));
            }

            switch (arrFunc.get(k)) {
                case "pi":
                    res = 3.14159265359;
                    break;
                case "e":
                    res = 2.71828182846;
                    break;
                case "sin":
                    res = Math.sin(num);
                    break;
                case "abs":
                    res = Math.abs(num);
                    break;
                case "acos":
                    res = Math.acos(num);
                    break;
                case "asin":
                    res = Math.asin(num);
                    break;
                case "atan":
                    res = Math.atan(num);
                    break;
                case "cbrt":
                    res = Math.cbrt(num);
                    break;
                case "ceil":
                    res = Math.ceil(num);
                    break;
                case "cos":
                    res = Math.cos(num);
                    break;
                case "exp":
                    res = Math.exp(num);
                    break;
                case "log":
                    res = Math.log(num);
                    break;
                case "floor":
                    res = Math.floor(num);
                    break;
                case "rint":
                    res = Math.rint(num);
                    break;
                case "signum":
                    res = Math.signum(num);
                    break;
                case "sinh":
                    res = Math.sinh(num);
                    break;
                case "sqrt":
                    res = Math.sqrt(num);
                    break;
                case "tan":
                    res = Math.tan(num);
                    break;
                case "tanh":
                    res = Math.tanh(num);
                    break;
                case "copySign":
                    res = Math.copySign(firstVal, secondVal);
                    break;
                case "min":
                    res = Math.min(firstVal, secondVal);
                    break;
                case "hypot":
                    res = Math.hypot(firstVal, secondVal);
                    break;
                case "pow":
                    res = Math.pow(firstVal, secondVal);
                    break;
                default:
                    isFunc = false;
            }

            if (isFunc) {
                arrFunc.set(k, String.valueOf(res));
                arrFunc.remove(k+1);
            }
        }

        // A recorded calculation results in the results array.
        int count = 0;
        for (int j = 0; j < arrValues.size(); j++) {
            if (arrValues.get(j).charAt(0) >= 'a' && arrValues.get(j).charAt(0) <= 'z'){
                arrValues.remove(j);
                arrValues.add(j, arrFunc.get(count));
                count++;
            }
        }
    }

    /** The method of bringing the necessary data in the right array.
     * @param tmp Variable to store the intermediate data.
     * @param arrOperators An array of mathematical operators.
     * @param arrTmp An array of intermediate data.
     * @param arrValues An array of results.
     */
    private void distributeValuesAndOperators(String tmp, VDYArrayList<String> arrOperators, VDYArrayList<String> arrTmp, VDYArrayList<String> arrValues) {
        // If the operator is not a pointer of a negative number, we put it into the array operators.
        for (int j = 0; j < tmp.length(); j++) {
            if (j != 0) {
                if (isOperator(tmp.charAt(j)) && !isOperator(tmp.charAt(j - 1))) {
                    arrOperators.add(tmp.charAt(j) + "");
                }
            }
        }

        // Put the values in an array from incoming data from operator to operator.
        for (int k = 0; k < tmp.length(); k++) {
            if (k != 0 && isOperator(tmp.charAt(k))) {
                arrTmp.add(tmp.substring(0, k));
                tmp = tmp.substring(k+1, tmp.length());
                k = 0;
            }
        }
        // Recorded the last value from the string into an array of values.
        arrTmp.add(tmp);

        // a recorded vote was taken resulting array operators and values in turn.
        for (int o = 0; o < arrTmp.size(); o++) {
            arrValues.add(arrTmp.get(o));
            if (o != arrTmp.size() - 1) {
                arrValues.add(arrOperators.get(o));
            }
        }
    }


    /** A method that returns a match with a char equal to a mathematical operator.
     * @param ch Char from the string.
     * @return Returns true in case of a match with a mathematical operator.
     */
    private boolean isOperator(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }


    /** Method is responsible for the product of mathematical operations.
     * @param firstDigit The intermediate data.
     * @param operator The variable responsible for mathematical actions.
     * @param secondDigit The intermediate data.
     * @return Returns the result of a mathematical operation.
     */
    private double mathAction(String firstDigit, String operator, String secondDigit){
        double digitA = getFractionalNum(firstDigit);
        double digitB = getFractionalNum(secondDigit);
        double res = 0;

        if (operator.equals("*")){
            res = digitA * digitB;
        }
        if (operator.equals("/")){
            res = digitA / digitB;
        }
        if (operator.equals("+")){
            res = digitA + digitB;
        }
        if (operator.equals("-")){
            res = digitA - digitB;
        }
        return res;
    }

    /** The method responsible for converting the string to a number.
     * @param num The incoming data is in row format.
     * @return Returns the number format double.
     */
    private double getFractionalNum(String num) {
        num = num.replace('(', ' ');
        num = num.replaceAll(" ", "");

        return Double.parseDouble(num);
    }

    /**@param tmp Variable to store the intermediate data.
     * @return Returns the result of the calculation.
     */
    private String calculateValues(VDYArrayList<String> tmp){
        doMathAction(tmp, "*", "/");
        doMathAction(tmp, "+", "-");
        return tmp.get(0);
    }

    /** The method makes the necessary calculations in accordance with the rules of mathematics.
     * @param tmp Variable to store the intermediate data.
     * @param fOperator Variable to store the operator.
     * @param sOperator Variable to store the operator.
     */
    private void doMathAction(VDYArrayList<String> tmp, String fOperator, String sOperator) {
        for (int i = 0; i < tmp.size(); i++) {
            if (tmp.get(i).equals(fOperator) || tmp.get(i).equals(sOperator)){
                tmp.set(i-1, String.valueOf(mathAction(tmp.get(i-1),tmp.get(i),tmp.get(i+1))));
                tmp.remove(i);
                tmp.remove(i);
                i = 0;
            }
        }
    }
}